# Chartix

This is wave visualizer that I created for analyzing audio.

Those charts that I found before could not handle huge amount of points (48000 point for 1 second of audio).

I use it for few second audio, when tried for 12 minute mp3 it worked, but laggy.

Here is demo: https://p11h5.csb.app/, try to zoom in (mouse wheel or touchpad + ctrl or just tachpad on mac),
scroll left and right, also there is ability to add selections (just click and pull cursor).

And code for this demo: https://codesandbox.io/s/elated-water-p11h5

If you want to use it - let me know creating an issue and I will write documentation.
