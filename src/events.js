const spliceFromBoundStore = (store, i) => {
  store.names.splice(i, 1)
  store.listeners.splice(i, 1)
  store.zIndexes.splice(i, 1)
}

export default class Events {
  boundListenersObjects = []
  boundListeners = []

  listeners = {}
  sortedKeys = {}

  constructor(element) {
    this.element = element
  }

  bindListener(object, name, listener, zIndex) {
    let index = this.boundListenersObjects.indexOf(object)
    if (index === -1) {
      index = this.boundListenersObjects.length
      this.boundListenersObjects[index] = object
      const {stop} = object
      const store = {names: [], listeners: [], zIndexes: [], stop}
      this.boundListeners[index] = store
      if (stop)
        object.stop = () => {
          stop.call(object)
          this.unbindListeners(store)
        }
      else
        object.stop = () => this.unbindListeners(store)
    }
    const store = this.boundListeners[index]
    if (store.listeners.indexOf(listener) === -1) {
      store.names.push(name)
      store.listeners.push(listener)
      store.zIndexes.push(zIndex)
    }
    this.addListener(name, listener, zIndex)
  }

  unbindListeners(store) {
    store.names.forEach((name, i) => {
      this.removeListener(name, store.listeners[i], store.zIndexes[i])
    })
  }

  removeBoundListener(object, name = null, listener = null) {
    const index = this.boundListenersObjects.indexOf(object)
    if (index === -1) return
    const store = this.boundListeners[index]
    if (listener) {
      const storeIndex = store.listeners.indexOf(listener)
      if (storeIndex !== -1) spliceFromBoundStore(store, storeIndex)
    } else if (name) {
      let storeIndex
      while ((storeIndex = store.names.indexOf(name)) !== -1)
        spliceFromBoundStore(store, storeIndex)
    }
    if (!name || store.names.length === 0) {
      this.boundListenersObjects.splice(index, 1)
      this.boundListeners.splice(index, 1)
      object.stop = store.stop
    }
  }

  addListener(name, listener, zIndex = null) {
    let listeners = this.listeners[name]
    if (!listeners) {
      this.listeners[name] = listeners = {}
      this.sortedKeys[name] = []
      if (this.element && this.element['on' + name] !== undefined)
        this.element.addEventListener(name, this.handleBrowserEvent)
    }
    if (!listeners[zIndex]) {
      listeners[zIndex] = []
      this.sortedKeys[name].push(zIndex)
      this.sortedKeys[name].sort((a, b) => a < b ? 1 : -1)
    }
    if (listeners[zIndex].indexOf(listener) === -1)
      listeners[zIndex].push(listener)
  }

  removeListener(name, listener, zIndex = null) {
    const listeners = this.listeners[name]
    if (!listeners || !listeners[zIndex]) return
    const index = listeners[zIndex].indexOf(listener)
    if (index === -1) return
    listeners[zIndex].splice(index, 1)
    if (listeners[zIndex].length === 0) {
      delete listeners[zIndex]
      this.sortedKeys[name].splice(this.sortedKeys[name].indexOf(zIndex), 1)
      if (Object.keys(listeners).length === 0) {
        delete this.listeners[name]
        delete this.sortedKeys[name]
        if (this.element && this.element['on' + name] !== undefined)
          this.element.removeEventListener(name, this.handleBrowserEvent)
      }
    }
  }

  handleBrowserEvent = (e) => {
    this.emitEvent(e.type, e)
  }

  emit(name, target, data = {}) {
    data.target = target
    data.type = name
    this.emitEvent(name, data)
  }

  emitEvent(name, event) {
    const typeListeners = this.listeners[name]
    if (!typeListeners) return

    let propagationStopped = false
    const stopPropagation = () => propagationStopped = true

    const keys = this.sortedKeys[name]
    for (let i = 0; i < keys.length; i++) {
      typeListeners[keys[i]].forEach(listener =>
        listener.call(null, event, stopPropagation)
      )
      if (propagationStopped)
        return
    }
  }
}
