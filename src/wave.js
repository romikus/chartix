import Component from './component'
import Path from './wave/path'
import {shallowEqual} from './utils'

export default class Wave extends Component {
  path = null
  pathArguments = null

  draw() {
    if (this.props.values.length === 0) return

    this.setupValuesArrays()
    const {width, height, offsetX, scaleX} = this.chart
    const pathArguments = {width, height, offsetX, scaleX, ...this.props}
    if (!this.path || !shallowEqual(this.pathArguments, pathArguments)) {
      this.path = new Path(pathArguments)
      this.pathArguments = pathArguments
    }

    this.drawer()
  }

  drawer() {
    if (this.props.draw) return this.props.draw(this.chart, this.path, this.props)

    this.chart.context.stroke(this.path)
    this.chart.context.fill(this.path)
  }

  setupValuesArrays() {
    const {values} = this.props
    const {width} = this.chart
    if (this.props.xValues)
      if (this.props.xValues.length === width) {
        if (this.props.lowValues.constructor === values.constructor)
          return
      } else if (Array.isArray(this.props.lowValues) && Array.isArray(values)) {
        this.props.xValues = new Uint16Array(width)
        this.props.lowValues.length = width
        this.props.highValues.length = width
        return
      }

    this.props.xValues = new Uint16Array(width)
    this.props.lowValues = new values.constructor(width)
    this.props.highValues = new values.constructor(width)
  }

  get highFactor() { return this.path.highFactor }
  get lowFactor() { return this.path.lowFactor }
}
