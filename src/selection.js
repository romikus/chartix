import Events from './events'
import {scaleX, unscaleX, contain, drawRect} from "./utils";
import Component from './component'

const defaults = {
  nearPixels: 3,
  removable: false,
  resizable: false,
  context: {
    strokeStyle: 'rgba(0, 188, 212, 0.3)',
    fillStyle: 'rgba(0, 188, 212, 0.1)',
  }
}

export default class Selection extends Component {
  removeReady = false
  resizeReady = false
  resizing = false
  cursorWas = ''
  nearPixels = 3
  x = null
  y = null
  width = null
  height = null
  events = new Events()

  get from() { return this.x }
  get to() { return this.x + this.width }
  set from(value) {
    this.width = this.width + this.x - value
    this.x = value
  }
  set to(value) {
    this.width = value - this.x
  }

  constructor(props) {
    super(Object.assign({}, defaults, props))
    this.icon = new Component({draw: props.drawIcon || this.drawIcon})
  }

  start(chart) {
    super.start(chart)
    this.icon.start(chart)
    chart.events.bindListener(this, 'mousedown', this.handleMousedown, this.props.zIndex)
    chart.events.bindListener(this, 'mouseup', this.handleMouseup)
    chart.events.bindListener(this, 'mousemove', this.handleMousemove)
  }

  draw() {
    const {chart} = this
    this.from = scaleX(chart, this.props.from)
    this.to = scaleX(chart, this.props.to)
    this.y = -1
    this.height = chart.height + 2
    drawRect(this)
    if (this.props.removable)
      drawRect(this.icon, this)
  }

  drawIcon = (context, icon, selection) => {
    const iconSign = '\u{02DF}'
    icon.x = scaleX(selection.chart, selection.props.to) - 20
    context.textBaseline = 'top'
    context.font = '30px Arial'
    if (!icon.y) {
      icon.y = 10
      const measurement = context.measureText(iconSign)
      icon.width = measurement.width
      icon.height = measurement.actualBoundingBoxDescent / 2
    }
    context.fillStyle = 'rgba(244, 67, 54, 0.6)'
    context.fillText(iconSign, icon.x, icon.y);
  }

  get interactive() {
    return this.props.resizable || this.props.removable
  }

  set interactive(value) {
    this.props.resizable = this.props.removable = value
  }

  handleMousedown = (e, stopPropagation) => {
    const resizeReady = this.props.resizable && this.resizeReady
    const removeReady = this.props.removable && this.removeReady
    if (!resizeReady && !removeReady)
      return

    stopPropagation()
    if (resizeReady)
      this.resizing = true
    else if (removeReady) {
      this.remove()
      this.chart.canvas.style.cursor = this.cursorWas
    }
  }

  handleMouseup = () => {
    if (this.resizing) {
      this.resizing = false
      this.events.emit('resizeStop', this)
    }
  }

  handleMousemove = (e) => {
    const {nearPixels} = this.props
    const {chart, from, to, icon} = this
    const {layerX: x, layerY: y} = e

    if (this.props.resizable && this.resizing) {
      let name
      if (Math.abs(x - from) < Math.abs(x - to)) {
        this.props.from = unscaleX(chart, x)
        name = 'from'
      } else {
        this.props.to = unscaleX(chart, x)
        name = 'to'
      }
      if (this.props.from > this.props.to) {
        const fromWas = this.props.from
        this.props.from = this.props.to
        this.props.to = fromWas
        name = 'both'
      }
      this.events.emit('resize', this, {name})
      return chart.draw()
    }

    if (this.props.removable && contain(icon, x, y)) {
      if (!this.removeReady) {
        this.removeReady = true
        this.cursorWas = this.chart.canvas.style.cursor
        chart.canvas.style.cursor = 'pointer'
      }
    } else if (
      this.props.resizable && (
        x > from - nearPixels && x < from + nearPixels ||
        x > to - nearPixels && x < to + nearPixels
      )
    ) {
      if (!this.resizeReady) {
        this.resizeReady = true
        this.cursorWas = this.chart.canvas.style.cursor
        chart.canvas.style.cursor = 'ew-resize'
      }
    } else if (this.props.resizable && this.resizeReady || this.props.removable && this.removeReady) {
      this.resizeReady = this.removeReady = false
      chart.canvas.style.cursor = this.cursorWas
    }
  }

  remove() {
    this.events.emit('remove', this)
    this.stop()
  }
}
