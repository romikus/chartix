import ImportedComponent from './component'
import ImportedEvents from './events'
import * as Utils from './utils'

export Scroll from './scroll'
export Zoom from './zoom'
export Wave from './wave'
export RangeSelector from './range_selector'
export Selection from './selection'
export Axis from './axis'
export const Component = ImportedComponent
export const Events = ImportedEvents
export const utils = Utils

export class Chart extends Component {
  constructor({wrapper, height = 300, scaleX = 1, offsetX = 0, items = []} = {}) {
    super({})
    this.wrapper = wrapper
    this.height = height
    this.canvas = document.createElement('canvas')
    this.canvas.height = this.height
    this.width = parseInt(window.getComputedStyle(this.wrapper).width)
    this.canvas.width = this.width
    this.events = new Events(this.canvas)
    this.context = this.canvas.getContext('2d')
    this.scaleX = scaleX
    this.offsetX = offsetX
    this.items = items
    this.started = false
    this.wrapper.appendChild(this.canvas)
  }

  add(item) {
    this.items.push(item)
    if (this.started)
      item.start && item.start(this)
  }

  remove(item) {
    super.remove(item)
  }

  start() {
    super.start(this)
    this.draw()
  }

  stop() {
    if (this.started)
      super.stop()
  }

  draw() {
    if (!this.started)
      return
    this.context.clearRect(0, 0, this.width, this.height)
    super.draw()
  }
}
