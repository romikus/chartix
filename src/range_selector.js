import Selection from './selection'
import Events from './events'
import Component from './component'
import {unscaleX} from './utils'

const defaults = {
  addable: false,
  resizable: false,
  removable: false,
  zIndex: 0,
  multiple: true,
}

export default class RangeSelector extends Component {
  static itemsKey = 'selections'
  events = new Events

  constructor(props) {
    super(Object.assign({}, defaults, props))
    if (props.interactive !== null)
      this.interactive = props.interactive
  }

  addSelection(props) {
    return this.add(new Selection(props))
  }

  add(selection) {
    selection.props.zIndex = this.props.zIndex + 1
    selection.props.resizable = this.props.resizable
    selection.props.removable = this.props.removable
    this.events.emit('addSelection', this, {selection})
    this.bindSelectionListeners(selection)
    super.add(selection)
    return selection
  }

  start(chart) {
    super.start(chart)
    this.bindListeners()
  }

  remove(selection, move = false) {
    if (!super.remove(selection))
      return
    selection.stop()
    this.events.emit('removeSelection', this, {selection, move})
    this.chart.draw()
  }

  handleMousedown = (e, stopPropagation) => {
    if (!this.addable) return
    if (this.props.multiple === false && this.selections.length)
      this.remove(this.selections[0], true)

    const unscaledX = unscaleX(this.chart, e.layerX)
    const selection = this.addSelection({from: unscaledX, to: unscaledX})

    selection.draw()
    selection.handleMousemove(e, stopPropagation)
    selection.handleMousedown(e, stopPropagation)
  }

  handleSelection = (e) => {
    this.events.emit(e.type + 'Selection', this, {selection: e.target, ...e})
  }

  handleRemoveSelection = ({target}) => {
    this.remove(target)
  }

  get addable() { return this.props.addable }
  get resizable() { return this.props.resizable }
  get removable() { return this.props.removable }
  get interactive() { return this.addable && this.resizable && this.removable }

  set interactive(value) { this.addable = this.resizable = this.removable = value }
  set addable(value) { this.props.addable = value }

  set resizable(value) {
    this.props.resizable = value
    this.selections.forEach(selection => selection.props.resizable = value)
  }

  set removable(value) {
    this.props.removable = value
    this.selections.forEach(selection => selection.props.removable = value)
  }

  bindListeners() {
    this.chart.events.bindListener(this, 'mousedown', this.handleMousedown, this.zIndex)
    this.selections.forEach(this.bindSelectionListeners, this)
  }

  bindSelectionListeners(selection) {
    selection.events.bindListener(selection, 'resize', this.handleSelection)
    selection.events.bindListener(selection, 'resizeStop', this.handleSelection)
    selection.events.bindListener(selection, 'remove', this.handleRemoveSelection)
  }
}
