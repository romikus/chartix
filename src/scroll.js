export default class Scroll {
  mouseX = null

  start(chart) {
    this.chart = chart
    this.canvas = chart.canvas
    chart.events.bindListener(this, 'scaleXChange', this.handleScaleChange)
    chart.events.bindListener(this, 'wheel', this.handleWheel)
    chart.events.bindListener(this, 'mousemove', this.handleMousemove)
    chart.events.bindListener(this, 'mouseout', this.handleMouseout)
  }

  handleScaleChange = ({was}) => {
    const target = this.mouseX === null ? this.chart.width / 2 : this.mouseX
    this.chart.offsetX = (this.chart.offsetX + target) / was * this.chart.scaleX - target
  }

  handleWheel = (e) => {
    if (e.deltaX === 0) return

    e.preventDefault()

    const was = this.chart.offsetX
    let offsetX = was + e.deltaX
    if (this.chart.offsetX !== offsetX) {
      this.chart.offsetX = offsetX
      this.chart.events.emit('offsetXChange', this, {was})
      this.chart.draw()
    }
  }

  handleMousemove = (e) => this.mouseX = e.layerX
  handleMouseout = () => this.mouseX = null
}
