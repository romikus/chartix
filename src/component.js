export default class Component {
  started = false
  chart = null

  constructor(props = {}) {
    this.props = props
    this.itemsKey = this.constructor.itemsKey || 'items'
    this[this.itemsKey] = props[this.itemsKey] || []
  }

  add(item) {
    this[this.itemsKey].push(item)
    if (this.started)
      item.start && item.start(this.chart)
  }

  remove(item) {
    const index = this[this.itemsKey].indexOf(item)
    if (index === -1)
      return false
    this[this.itemsKey].splice(index, 1)
    item.stop && item.stop()
    return true
  }

  start(chart) {
    if (this.started)
      return
    this.started = true
    this.chart = chart
    this[this.itemsKey].forEach(item => item.start && item.start(chart))
  }

  stop() {
    if (this.started) {
      this.started = false
      this[this.itemsKey].forEach(item => item.stop && item.stop())
    }
  }

  draw() {
    if (!this.started)
      return
    this[this.itemsKey].forEach(item => item.draw && item.draw())
  }
}
