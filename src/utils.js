export const scaleX = ({scaleX, offsetX}, x) => {
  return x * scaleX - offsetX
}

export const unscaleX = ({scaleX, offsetX}, x) => {
  return (x + offsetX) / scaleX
}

export const contain = (obj, x, y) =>
  x >= obj.x && x <= obj.x + obj.width && y >= obj.y && y <= obj.y + obj.height

export const drawRect = (obj, ...args) => {
  const {context} = obj.chart
  const {draw, context: style} = obj.props
  if (draw) return draw(context, obj, ...args)
  Object.assign(context, style)
  context.beginPath()
  context.rect(obj.x, obj.y, obj.width, obj.height)
  context.stroke()
  context.fill()
}

export const shallowEqual = (a, b) => {
  if (a === b) return true
  if (!a || !b || Object.keys(a).length !== Object.keys(b).length) return false

  for (let key in a)
    if (a[key] !== b[key])
      return false

  return true
}

