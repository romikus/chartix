export default class Path extends Path2D {
  constructor({
    values, minValue = -1, maxValue = 1, scaleX, offsetX, width, height,
    stretch, top = 0, xValues, lowValues, highValues, fill
  }) {
    super()
    this.values = values
    this.minValue = minValue
    this.maxValue = maxValue
    this.scaleX = scaleX
    this.offsetX = offsetX
    this.width = width
    this.height = height
    this.stretch = stretch
    this.xValues = xValues
    this.lowValues = lowValues
    this.highValues = highValues
    this.fill = fill
    this.halfHeight = height / 2
    this.top = top
    this.draw()
  }

  draw() {
    const {
      values, minValue, maxValue, scaleX, offsetX, width, stretch,
      xValues, lowValues, highValues
    } = this
    let valueI = 0
    const last = values.length - 1
    const showingCount = values.length / scaleX
    const dense = showingCount > width
    let min, max, minI, maxI
    let toI = Math.floor(offsetX * showingCount / width) - 1
    let x = 0
    for (; x < width; x++) {
      const fromI = toI
      if (fromI > last)
        break

      toI = Math.floor((offsetX + x + 1) * showingCount / width)

      if (fromI >= 0) {
        xValues[valueI] = x
        if (dense) {
          max = minValue
          min = maxValue
          for (let i = fromI; i < toI; i++) {
            if (dense && min > values[i]) {
              min = values[i]
              minI = i
            }
            if (max < values[i]) {
              max = values[i]
              maxI = i
            }
          }
          lowValues[valueI] = values[minI]
          highValues[valueI++] = values[maxI]
        } else {
          highValues[valueI++] = values[fromI]
        }
      }
    }

    if (valueI === 0)
      return

    let highFactor = 1
    let lowFactor = 1
    if (stretch) {
      max = minValue
      highValues.forEach(value => {
        if (max < value) max = value
      })
      highFactor = maxValue / max
      this.highFactor = highFactor

      min = maxValue;
      (dense ? lowValues : highValues).forEach(value => {
        if (min > value) min = value
      })
      lowFactor = minValue / min
      this.lowFactor = lowFactor
    }

    if (this.fill)
      this.lineTo(xValues[0] - 1, this.halfHeight)

    if (dense) {
      let count = 0
      for (let i = 0; i < valueI; i++)
        if (highValues[i] < 0)
          count++
      if (count / valueI < 0.3) {
        for (let i = 0; i < valueI; i++)
          this.drawPoint(i, highValues, highFactor, lowFactor)
        for (let i = valueI - 1; i >= 0; i--)
          this.drawPoint(i, lowValues, highFactor, lowFactor)
      } else {
        for (let i = 0; i < valueI; i++) {
          if (highValues[i] > 0)
            this.drawPoint(i, highValues, highFactor, lowFactor)
          if (lowValues[i] < 0)
            this.drawPoint(i, lowValues, highFactor, lowFactor)
        }
        this.lineTo(x, this.halfHeight)
      }
    } else {
      for (let i = 0; i < valueI; i++)
        this.drawPoint(i, highValues, highFactor, lowFactor)
      if (this.fill)
        this.lineTo(x, this.halfHeight)
    }
  }

  drawPoint(i, array, highFactor, lowFactor) {
    let value = array[i]
    value *= (value > 0 ? highFactor : lowFactor) || 1
    this.lineTo(this.xValues[i], this.valueToY(value))
  }

  valueToY(value) {
    return this.halfHeight - this.halfHeight * value + this.top
  }
}
