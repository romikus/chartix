export default class Zoom {
  constructor(options = {}) {
    this.scaleFactor = 0.01
    Object.assign(this, options)
  }

  start(chart) {
    this.chart = chart
    this.canvas = chart.canvas
    chart.events.bindListener(this, 'wheel', this.handleWheel)
  }

  handleWheel = (e) => {
    if (!e.ctrlKey)
      return

    e.preventDefault()
    const {scaleX} = this.chart
    this.chart.scaleX -= e.deltaY * this.scaleFactor * this.chart.scaleX
    this.chart.events.emit('scaleXChange', this, {was: scaleX})
    this.chart.draw()
  }
}
