import Component from './component'
import {shallowEqual} from './utils'

export default class Axis extends Component {
  constructor({
    from,
    to,
    side = 'bottom',
    textSize = 14,
    font = `${textSize}px Arial`,
    color = 'white',
    textSpace = textSize,
    size = 100,
    formatValue,
    minSize,
    ticks = {},
    top = 0,
    left = 0,
    right = 0,
    bottom = 0,
  }) {
    super()
    Object.assign(this, {
      from, to, ticks, textSize, textSpace, font, color,
      formatValue, size, minSize, top, left, right, bottom
    })
    this.step = this.textPos = this.fromI = this.toI = this.path = this.pathDeps = null
    this.isTop = side === 'top'
    this.isLeft = side === 'left'
    this.isBottom = side === 'bottom'
    this.horizontal = this.isTop || this.isBottom
    this.isTopOrLeft = this.isTop || this.isLeft
  }

  draw() {
    const {chart, top, left, right, bottom, ticks} = this
    const {scaleX, offsetX} = chart
    const width = chart.width - left - right
    const height = chart.height - top - bottom
    this.setSize(width, height)
    this.setStep(scaleX, width, height)
    this.setTextPos(width, height)
    this.setFromI(offsetX)
    this.setToI(offsetX, width, height)

    this.drawValues(this)

    if (!ticks) return
    this.checkPath({offsetX, width, height})
    this.drawTicks(this)
  }

  setSize(width, height) {
    if (!this.minSize) return this.size
    const dimension = this.horizontal ? width : height

    let size = this.minSize
    while (dimension % size)
      size++
    return this.size = size
  }

  setStep(scaleX, width, height) {
    return this.step = this.size / (this.horizontal ? scaleX * width : height) * (this.to - this.from)
  }

  setTextPos(width, height) {
    return this.textPos =
      this.isTopOrLeft ? this.textSpace : (this.isBottom ? height : width) - this.textSpace
  }

  setFromI(offsetX) {
    return this.fromI = this.horizontal ? ~~(offsetX / this.size) : 0
  }

  setToI(offsetX, width, height) {
    return this.toI = this.horizontal ? (offsetX + width) / this.size : height / this.size
  }

  getX(i) {
    return i * this.size - this.chart.offsetX
  }

  getY(i) {
    return i * this.size + this.top
  }

  getValue(i) {
    return this.from + this.step * i
  }

  checkPath(deps) {
    if (shallowEqual(this.pathDeps, deps)) return
    this.path = this.createPath()
    this.pathDeps = deps
  }

  createPath() {
    const {ticks: {offset = 3, size = 5}, fromI, toI, textPos} = this
    const path = new Path2D()

    const tick1 = textPos + (this.isTopOrLeft ? offset : -offset)
    const tick2 = tick1 + (this.isTopOrLeft ? size : -size)

    if (this.horizontal) {
      for (let i = fromI; i <= toI; i++) {
        const x = this.getX(i)
        path.moveTo(x, tick1)
        path.lineTo(x, tick2)
      }
    } else {
      for (let i = fromI; i <= toI; i++) {
        const y = this.getY(i)
        path.moveTo(tick1, y)
        path.lineTo(tick2, y)
      }
    }

    return path
  }

  drawValues() {
    const {chart: {context}, formatValue, fromI, toI, textPos, isLeft} = this
    context.font = this.font
    context.fillStyle = this.color
    context.textBaseline = (
      this.isBottom ? 'top' : this.isTop ? 'bottom' : 'middle'
    )
    if (this.horizontal) {
      for (let i = fromI; i <= toI; i++) {
        const x = this.getX(i)
        const value = this.getValue(i)
        const text = formatValue ? formatValue(value) : value
        const halfWidth = context.measureText(text).width / 2
        context.fillText(text, x - halfWidth, textPos)
      }
    } else {
      for (let i = fromI; i <= toI; i++) {
        const y = this.getY(i)
        const value = this.getValue(i)
        const text = formatValue ? formatValue(value) : value
        const x = textPos - (isLeft ? context.measureText(text).width : 0)
        context.fillText(text, x, y)
      }
    }
  }

  drawTicks() {
    const {context} = this.chart
    context.strokeStyle = this.ticks.color || this.color
    context.stroke(this.path)
  }
}
