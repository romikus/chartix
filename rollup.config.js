import babel from 'rollup-plugin-babel'

export default {
  input: 'src/chart.js',
  output: {
    file: 'bundle.js',
    format: "umd",
    name: "chartix"
  },
  plugins: [
    babel({
      presets: [
        '@babel/preset-env'
      ],
      plugins: [
        "@babel/plugin-proposal-class-properties",
        "@babel/plugin-proposal-export-default-from"
      ],
      exclude: 'node_modules/**'
    })
  ]
}
